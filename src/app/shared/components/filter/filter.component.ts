import { OnInit, Component, EventEmitter, Input, Output } from '@angular/core';

import { Filter } from '../../models';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent {
  @Input() filter: Filter;
  @Output() filterChanged = new EventEmitter();

  constructor(
  ) { }

  onChange(event) {
    this.filterChanged.emit({ name: this.filter.name, isChecked: event.target.checked });
  }
}
