import { DomSanitizer } from '@angular/platform-browser';
import { OnInit, Component, Input } from '@angular/core';

import { Video } from '../../models';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  @Input() video: Video;
  @Input() sources: any[];

  constructor(private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.setTemplate();
  }

  setTemplate() {
    const source = this.sources.find(i => i.source === this.video.source);
    if (source) {
      this.video.template = source.template;
      this.replaceTemplatePlaceHolders();
      this.sanitize();
    } else {
      this.video.template = 'Video post is missing !';
    }
  }

  replaceTemplatePlaceHolders() {
    if (this.video['url']) {
      this.video.template = this.video.template.replace('{{url}}', this.video['url']);
    }
    else if (this.video['videoId']) {
      this.video.template = this.video.template.replace('{{videoId}}', this.video['videoId']);
    }
  }

  sanitize() {
    this.video.template = this.sanitizer.bypassSecurityTrustHtml(this.video.template);
  }

}
