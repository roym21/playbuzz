import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VideoComponent, FilterComponent } from './components';
import { NumberFormatPipe } from './pipes';
import {  VideoService } from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        VideoComponent,
        FilterComponent,
        NumberFormatPipe
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        VideoComponent,
        FilterComponent,
        NumberFormatPipe
    ],
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                VideoService
            ]
        };
    }
}