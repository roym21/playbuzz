import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'numberFormat',
    pure: false
})
export class NumberFormatPipe implements PipeTransform {
    transform(num: number): any {
        let formattedNumber;

        if (num >= 1000000000) {
            formattedNumber = (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
        } else if (num >= 1000000) {
            formattedNumber = (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
        } else if (num >= 1000) {
            formattedNumber = (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
        } else {
            formattedNumber = num;
        }
        return formattedNumber;
    }
}