
export class Video {
    title: string;
    type: string;
    source: string;
    template: any;
    views: number;
}