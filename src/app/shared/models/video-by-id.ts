import { Video } from './';

export class VideoById extends Video {
    videoId: string;
}