import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import { environment } from '../../../environments/environment';

@Injectable()

export class VideoService {

    constructor(
        private http: Http
    ) { }

    getVideos(): Promise<any> {
        return this.http
            .get(environment.videosUrl)
            .toPromise()
            .then(response => {
                const res = response.json();
                return res;
            })
            .catch(error => {
                this.handleError(error);
            });
    }

    getSources(): Promise<any> {
        return this.http
            .get(environment.sourcesUrl)
            .toPromise()
            .then(response => {
                const res = response.json();
                return res;
            })
            .catch(error => {
                this.handleError(error);
            });
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
