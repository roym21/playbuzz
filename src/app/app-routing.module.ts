import { NgModule } from '@angular/core';
import { RouterModule, Route, Routes, PreloadAllModules, CanActivate } from '@angular/router';

const ROUTES: Route[] = [
    {
        path: '',
        children: [
            { path: '', loadChildren: './feed/feed.module#FeedModule' }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(ROUTES, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }