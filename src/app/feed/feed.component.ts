import { OnInit, Component } from '@angular/core';
import { VideoService } from '../shared/services';
import { Video, Filter } from '../shared/models';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  sources: any[];
  videos: Video[];
  filters: Filter[];

  constructor(
    private videoService: VideoService
  ) { }

  ngOnInit() {
    Promise.all([this.videoService.getVideos(), this.videoService.getSources()]).then((data) => {
      if (data) {
        this.videos = data[0].items;
        this.sources = data[1];
        this.setFilters();
      }
    });
  }

  setFilters() {
    this.filters = this.sources
      .filter(s => this.videos.map(v => v.source).indexOf(s.source) > -1).
      map(i => { return { name: i.source, isChecked: true }; });
  }

  onFilterChanged(filter: Filter) {
    const filterObj = this.filters.find(i => i.name === filter.name).isChecked = filter.isChecked;
  }

  isFiltered(video: Video) {
    return this.filters.find(f => f.name === video.source).isChecked === false;
  }
}
