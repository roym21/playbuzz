import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { FeedRoutingModule } from './feed-routing.module';
import { FeedComponent } from './feed.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FeedRoutingModule
    ],
    declarations: [FeedComponent]
})
export class FeedModule { }