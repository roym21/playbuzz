export const environment = {
  production: true,
  videosUrl: "https://cdn.playbuzz.com/content/feed/items",
  sourcesUrl: "assets/sources.json",
};
