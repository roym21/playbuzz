import { DudaPage } from './app.po';

describe('duda App', () => {
  let page: DudaPage;

  beforeEach(() => {
    page = new DudaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
